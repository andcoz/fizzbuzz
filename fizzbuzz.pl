#!/usr/bin/perl
#
#  Copyright 2019, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU
#  General Public License. You may redistribute and/or modify this
#  program under the terms of the version 2 of that license as
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

use v5.016;
use strict;
use warnings;

for (1..100) {
    my $d = "";

    $d .= "Fizz" if $_ % 3 == 0;
    $d .= "Buzz" if $_ % 5 == 0;

    $d = $_ if length $d == 0;

    say $d;
}
