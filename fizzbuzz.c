/*
 *  Copyright 2019, Andrea Cozzolino <andcoz@gmail.com>
 *
 *  This program is free software which I release under the GNU
 *  General Public License. You may redistribute and/or modify this
 *  program under the terms of the version 2 of that license as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.  Version 2 is in the
 *  COPYRIGHT file in the top level directory of this distribution.
 *
 *  Please, note that re-distributing this library under the
 *  version 3 of GNU GPL is explicitly forbidden.
 */

#include <stdio.h>
#include <string.h>

/* required by bzero function */
/* #include <strings.h> */

#define DESCRIPTION_LEN 9

/* this function is not thread safe */
/* this function returns internal (static) variable */
char *fizzbuzz(int current) {
  static char description[DESCRIPTION_LEN];

  description[0] = '\0';
  /* bzero(description, DESCRIPTION_LEN * sizeof(char)); */

  if (current % 3 == 0) {
    strcpy(description, "Fizz");
  }  
  if (current % 5 == 0) {
    strcat(description, "Buzz");
  }

  if (strlen(description) == 0)  {
    snprintf(description, DESCRIPTION_LEN * sizeof(char), "%d", current);
  }

  return description;
}

int main(int argc, char **argv) {

  for (int counter = 1; counter <= 100; counter++) {
    puts(fizzbuzz(counter));
  }
  
  return 0; 
}
