#
#  Copyright 2019, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU
#  General Public License. You may redistribute and/or modify this
#  program under the terms of the version 2 of that license as
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

.PHONY: all run

JAVAC = javac
GRAPHWIZ = dot -T png

%.class: %.java
	$(JAVAC) $<

%.png: %.graphviz
	$(GRAPHWIZ) -o $@ $<

all: fizzbuzz fizzbuzz_printf fizzbuzz_cpp fizz/buzz/FizzBuzz.class fizz/buzz/ModernFizzBuzz.class automata/finite-state-automaton.png fizz/buzz/CountingFizzBuzz.class

clean:
	find . -type f -name \*~ -delete
	rm -f fizzbuzz fizzbuzz_printf fizzbuzz_cpp
	rm -f fizz/buzz/*.class
	rm -f automata/finite-state-automaton.png

run: all
	./fizzbuzz
	./fizzbuzz_printf
	./fizzbuzz_cpp
	./fizzbuzz.sh
	./fizzbuzz.bash
	./fizzbuzz.tcsh
	./fizzbuzz.pl
	java fizz.buzz.FizzBuzz
	java fizz.buzz.ModernFizzBuzz
	java fizz.buzz.CountingFizzBuzz

check: all
	@echo -n "./fizzbuzz --> " ; ./fizzbuzz > /dev/null ; echo $$?
	@echo -n "./fizzbuzz_printf --> " ; ./fizzbuzz_printf > /dev/null ; echo $$?
	@echo -n "./fizzbuzz_cpp --> " ; ./fizzbuzz_cpp > /dev/null ; echo $$?
	@echo -n "./fizzbuzz.sh --> " ; ./fizzbuzz.sh > /dev/null ; echo $$?
	@echo -n "./fizzbuzz.bash --> " ; ./fizzbuzz.bash > /dev/null ; echo $$?
	@echo -n "./fizzbuzz.tcsh --> " ; ./fizzbuzz.tcsh > /dev/null ; echo $$?
	@echo -n "./fizzbuzz.pl --> " ; ./fizzbuzz.pl > /dev/null ; echo $$?
	@echo -n "java fizz.buzz.FizzBuzz --> " ; java fizz.buzz.FizzBuzz > /dev/null ; echo $$?
	@echo -n "java fizz.buzz.ModernFizzBuzz --> " ; java fizz.buzz.ModernFizzBuzz > /dev/null ; echo $$?
	@echo -n "java fizz.buzz.CountingFizzBuzz --> " ; java fizz.buzz.CountingFizzBuzz > /dev/null ; echo $$?

