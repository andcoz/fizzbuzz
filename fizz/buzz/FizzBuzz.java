/*
 *  Copyright 2019, Andrea Cozzolino <andcoz@gmail.com>
 *
 *  This program is free software which I release under the GNU
 *  General Public License. You may redistribute and/or modify this
 *  program under the terms of the version 2 of that license as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.  Version 2 is in the
 *  COPYRIGHT file in the top level directory of this distribution.
 *
 *  Please, note that re-distributing this library under the
 *  version 3 of GNU GPL is explicitly forbidden.
 */

package fizz.buzz;

public class FizzBuzz {

    protected static class FizzBuzzNumber {
        private static final String FIZZ = "Fizz";
        private static final String BUZZ = "Buzz";
        private static final String FIZZBUZZ = "FizzBuzz";

        private int value;
        private boolean isFizz;
        private boolean isBuzz;

        public FizzBuzzNumber(int number) {
            value = number;
            isFizz = number % 3 == 0;
            isBuzz = number % 5 == 0;
        }

        public boolean isFizz() {
            return isFizz;
        }

        public boolean isBuzz() {
            return isBuzz;
        }

        public boolean isFizzBuzz() {
            return isBuzz() && isFizz();
        }

        public int getValue() {
            return value;
        }

        @Override
        public String toString() {
            if (isFizzBuzz()) {
                return FIZZBUZZ;
            } else if (isFizz()) {
                return FIZZ;
            } else if (isBuzz()) {
                return BUZZ;
            } /* else */ {
                return Integer.toString(value);
            }
        }
    }

    public static void main(String[] args) {
        for (int counter = 1; counter <= 100; counter++) {
            FizzBuzzNumber number = new FizzBuzzNumber(counter);
            System.out.println(number);
        }
    }
}
