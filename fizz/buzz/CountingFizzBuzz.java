/*
 *  Copyright 2022, Andrea Cozzolino <andcoz@gmail.com>
 *
 *  This program is free software which I release under the GNU
 *  General Public License. You may redistribute and/or modify this
 *  program under the terms of the version 2 of that license as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.  Version 2 is in the
 *  COPYRIGHT file in the top level directory of this distribution.
 *
 *  Please, note that re-distributing this library under the
 *  version 3 of GNU GPL is explicitly forbidden.
 */
package fizz.buzz;

public class CountingFizzBuzz {
    private static final String EMPTY = "";
    private static final String FIZZ = "Fizz";
    private static final String BUZZ = "Buzz";
    
    public static void main(String[] args) {
        int threeModulo = 1;
        int fiveModulo = 1;
        int counter = 1;

        while (counter <= 100) {
            String response = EMPTY;  

            if (threeModulo == 0)
                response = FIZZ;
            if (fiveModulo == 0)
                response += BUZZ;
            if (response == EMPTY)
                response = Integer.toString(counter);
            
            System.out.println(response);

            counter++;
            if (++threeModulo == 3)
                threeModulo = 0;
            if (++fiveModulo == 5)
                fiveModulo = 0;
        }
    }
}
