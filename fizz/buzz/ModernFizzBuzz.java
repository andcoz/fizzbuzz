/*
 *  Copyright 2019, Andrea Cozzolino <andcoz@gmail.com>
 *
 *  This program is free software which I release under the GNU
 *  General Public License. You may redistribute and/or modify this
 *  program under the terms of the version 2 of that license as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.  Version 2 is in the
 *  COPYRIGHT file in the top level directory of this distribution.
 *
 *  Please, note that re-distributing this library under the
 *  version 3 of GNU GPL is explicitly forbidden.
 */

package fizz.buzz;

import java.util.stream.Stream;

public class ModernFizzBuzz {
    private static final String FIZZ = "Fizz";
    private static final String BUZZ = "Buzz";
    private static final String FIZZBUZZ = "FizzBuzz";

    public static void main(String[] args) {
        Stream
            .iterate(Integer.valueOf(1), (f) -> f + 1)
            .limit(100)
            .map(i -> i % 15 == 0 ? FIZZBUZZ : ( i % 5 == 0 ? BUZZ : ( i % 3 == 0 ? FIZZ : Integer.toString(i) ) ) )
            .forEach(System.out::println);
    }
}
