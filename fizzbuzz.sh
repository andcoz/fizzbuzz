#!/bin/sh
#
#  Copyright 2019, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU
#  General Public License. You may redistribute and/or modify this
#  program under the terms of the version 2 of that license as
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

for COUNTER in $(seq 1 100) ; do 

    DESCRIPTION=""
    [ $(( COUNTER % 3 )) -eq 0 ] && DESCRIPTION="Fizz"
    [ $(( COUNTER % 5 )) -eq 0 ] && DESCRIPTION="${DESCRIPTION}Buzz"
    [ -z "${DESCRIPTION}" ] && DESCRIPTION="${COUNTER}"

    echo "${DESCRIPTION}"

done
