#!/bin/tcsh
#
#  Copyright 2022, Andrea Cozzolino <andcoz@gmail.com>
#
#  This program is free software which I release under the GNU
#  General Public License. You may redistribute and/or modify this
#  program under the terms of the version 2 of that license as
#  published by the Free Software Foundation.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.  Version 2 is in the
#  COPYRIGHT file in the top level directory of this distribution.
#
#  Please, note that re-distributing this library under the
#  version 3 of GNU GPL is explicitly forbidden.
#

foreach COUNTER (`seq 1 100`)

    set DESCRIPTION = ""

    @ m3 = $COUNTER % 3
    [ $m3 == 0 ] && set DESCRIPTION = "Fizz"

    @ m5 = $COUNTER % 5
    [ $m5 == 0 ] && set DESCRIPTION = "${DESCRIPTION}Buzz"

    [ -z "${DESCRIPTION}" ] && set DESCRIPTION = "${COUNTER}"

    echo "${DESCRIPTION}"
    
end
