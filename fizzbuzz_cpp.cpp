/*
 *  Copyright 2019, Andrea Cozzolino <andcoz@gmail.com>
 *
 *  This program is free software which I release under the GNU
 *  General Public License. You may redistribute and/or modify this
 *  program under the terms of the version 2 of that license as
 *  published by the Free Software Foundation.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.  Version 2 is in the
 *  COPYRIGHT file in the top level directory of this distribution.
 *
 *  Please, note that re-distributing this library under the
 *  version 3 of GNU GPL is explicitly forbidden.
 */

#include <iostream>

using namespace std;

int main(int argc, char **argv) {
  for (int counter = 1; counter <= 100; counter++) {
    bool done = false;
  
    if (counter % 3 == 0) {
      cout << "Fizz";
      done = true;
    }
    if (counter % 5 == 0) {
      cout << "Buzz";
      done = true;
    }

    if (! done)  {
      cout << counter;
    }

    cout << "\n";
  }

  return 0;
}
