# Fizz Buzz implementations

A collection of [Fizz Buzz](https://en.wikipedia.org/wiki/Fizz_buzz)
implementations in different programming languages.

* Shell Scripts
    * [fizzbuzz.sh](./fizzbuzz.sh): an implementation for posix
    `sh`, based on modulus operation and string concatenation.
    * [fizzbuzz.bash](./fizzbuzz.bash): an implementation for GNU
    `bash`, based on modulus operation and string concatenation.
    * [fizzbuzz.tcsh](./fizzbuzz.tcsh): an implementation for `tcsh`,
    based on modulus operation and string concatenation.
* C
    * [fizzbuzz.c](./fizzbuzz.c): an implementation for posix `C`,
    based on modulus operation and string concatenation.
    * [fizzbuzz_printf.c](./fizzbuzz_printf.c): an implementation
    for posix `C`, based on modulus operation and `printf` formats.
* Perl
    * [fizzbuzz.pl](./fizzbuzz.pl)
* C++
    * [fizzbuzz_cpp.cpp](./fizzbuzz_cpp.cpp)
* Java
    * [fizz.buzz.FizzBuzz](./fizz/buzz/FizzBuzz.java): an
    implementation in `Java` language, based on modulus operation
    and string concatenation. Uses a simple FizzBuzz class to
    model the integer rappresentation according to game rules.
    * [fizz.buzz.ModernFizzBuzz](./fizz/buzz/ModernFizzBuzz.java)an
    implementation in `Java` language, based on modulus operation
    and stream manipulation.
    * [fizz.buzz.CountingFizzBuzz](./fizz/buzz/CountingFizzBuzz.java):
    an implementation in `Java` language that does not uses modulus
    operation but only sums.
* Other
    * [Finite State Automaton](./automata/finite-state-automaton.png):
    this automaton accepts any integer number. The name of the
    accepting state is the "word" that the player should tell for that
    number.

Copyright 2019-2022, "Andrea Cozzolino" <andcoz@gmail.com>

